console.log("Service Worker Loaded...");

self.addEventListener("push", e => {
  const data = e.data.json();
  console.log("Push Recieved...");
  self.registration.showNotification(data.title, {
    body: "Notified by my app :)",
    icon: "https://design.firefox.com/photon/images/product-identity-assets/firefox.png"
  });
});