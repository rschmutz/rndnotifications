var express = require('express');
const webpush = require("web-push");
const bodyParser = require("body-parser");
const path = require("path");

var app = express();

app.use(bodyParser.json());

const publicVapidKey = "BG3gLxOWCQLuw2Ffz2t72KlPnVC4tnfjmu0iq-Q8UO-PKLrsTuPk4lzm5TD5V9D4y2UPHSSjWCtO_JspxIE9syg";
const privateVapidKey = "PfOeJQe-gNZQL57HqUID3nixdP3LLgu1iVtyD-RTGxw";

webpush.setVapidDetails(
  "mailto:raphael.schmutz@e-teach.ch",
  publicVapidKey,
  privateVapidKey
);

app.get('/', function(req, res) {
  res.send('Mon super serveur NodeJS');
});

let subscriptions = [];

// Subscribe Route
app.post("/subscribe", (req, res) => {

  // Get pushSubscription object
  const subscription = req.body;

  subscriptions.push(subscription);

  // Send 201 - resource created
  res.status(201).json({});

});

app.get("/send", (req, res) => {
  console.log("Send notifications !");

  // Create payload
  const payload = JSON.stringify({ title: "Push Test" });

  subscriptions.forEach(subscription => {
      webpush
        .sendNotification(subscription, payload)
        .catch(err => console.error(err));
  });

  res.status(201).json({"complete": true});
  
});

app.use('/admin', express.static('admin'));
app.use('/app', express.static('public'));


const port = 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));
